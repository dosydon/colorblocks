package com.badlogic.androidgames.mrnom;

import com.badlogic.androidgames.framework.Pixmap;
import com.badlogic.androidgames.framework.Sound;

public class Assets {
    public static Pixmap gameOver;
    public static Pixmap menu_background;
    public static Pixmap soundenabled;
    public static Pixmap sounddisabled;
    public static Pixmap pause2;
    public static Pixmap ready2;
    public static Pixmap backtomenu;
    public static Pixmap cleared;
    public static Pixmap pause;
    public static Sound click;
    public static Sound eat;
    public static Sound bitten;
}
