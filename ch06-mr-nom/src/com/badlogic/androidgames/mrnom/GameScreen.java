package com.badlogic.androidgames.mrnom;

import java.util.List;

import android.graphics.Color;
import android.util.Log;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input.TouchEvent;
import com.badlogic.androidgames.framework.Pixmap;
import com.badlogic.androidgames.framework.Screen;

public class GameScreen extends Screen {
    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver,
        Cleared
    }
    
    GameState state = GameState.Ready;
    PanelWorld world;
    
    public GameScreen(Game game) {
        super(game);
        world = new PanelWorld();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        if(state == GameState.Ready)
            updateReady(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
        if(state == GameState.Cleared)
            updateCleared(touchEvents); 
    }
    
    private void updateReady(List<TouchEvent> touchEvents) {
       
    	if(touchEvents.size() > 0)
            state = GameState.Running;
    }
    
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {        
    	
    	int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x < 64 && event.y < 64) {
                    if(Settings.soundEnabled)
                        Assets.click.play(1);
                    state = GameState.Paused;
                    return;
                }
            }
            if(event.type == TouchEvent.TOUCH_DOWN) {
            	for(int j = 0; j < PanelWorld.panelarray.length; j++){
            		for(int k = 0;k < PanelWorld.panelarray[0].length;k++){
            			if(inBounds(event.x,event.y,j*64,k*64+160,(j+1)*64,(k+1)*64+160)){
            				PanelWorld.panelarray[j][k].kill();
            			}
            		}
            	}
            }
        }
        
        world.update(deltaTime);
        if(world.gameOver) {
            if(Settings.soundEnabled)
                Assets.bitten.play(1);
            state = GameState.GameOver;
        }
        if(world.cleared) {
            state = GameState.Cleared;
        }
    }
    
    private void updatePaused(List<TouchEvent> touchEvents) {
        
    	int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        if(Settings.soundEnabled)
                            Assets.click.play(1);
                        state = GameState.Running;
                        return;
                    }                    
                    if(event.y > 148 && event.y < 196) {
                        if(Settings.soundEnabled)
                            Assets.click.play(1);
                        game.setScreen(new MainMenuScreen(game));                        
                        return;
                    }
                }
            }
        }
    }
    
    private void updateGameOver(List<TouchEvent> touchEvents) {
    	
    	int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                   event.y >= 200 && event.y <= 264) {
                    if(Settings.soundEnabled)
                        Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }
    
    private void updateCleared(List<TouchEvent> touchEvents) {
    	if(touchEvents.size() > 0)
    		 game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        
        g.clear(Color.WHITE);
        drawWorld(world);
        if(state == GameState.Ready) 
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
        if(state == GameState.Cleared)
            drawClearedUI();
                        
    }
    
    private void drawWorld(PanelWorld world2) {
        Graphics g = game.getGraphics();
        for(int i = 0; i < PanelWorld.panelarray.length;i++){
        	for(int j = 0; j < PanelWorld.panelarray[0].length;j++){
        		int color = PanelWorld.panelarray[i][j].color;
                int x = PanelWorld.panelarray[i][j].x * 64;
                int y = PanelWorld.panelarray[i][j].y * 64+160;
                if(!PanelWorld.panelarray[i][j].isdead){
                	g.drawRect(x, y, 64, 64, color);
                }
        	}
        }
        for(int i = 0; i < PanelWorld.num_color0;i++){
        	g.drawRect(128+i*32, 0, 24, 24, PanelWorld.COLOR0);
        }
        for(int i = 0; i < PanelWorld.num_color1;i++){
        	g.drawRect(128+i*32, 32, 24, 24, PanelWorld.COLOR1);
        }
        for(int i = 0; i < PanelWorld.num_color2;i++){
        	g.drawRect(128+i*32, 64, 24, 24, PanelWorld.COLOR2);
        }
    }
    
    private void drawReadyUI() {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.ready2, 0, 96);
    }
    
    private void drawRunningUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.pause2,0,0);
        
    }
    
    private void drawPausedUI() {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.pause, 0, 96);
    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.gameOver, 0, 96);
        g.drawPixmap(Assets.backtomenu, 128, 200);
    }
    
    private void drawClearedUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.cleared, 0, 96);
    }
    
    
    
    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
        
    }

    @Override
    public void resume() {
        
    }

    @Override
    public void dispose() {
        
    }
    
    public boolean inBounds(int x, int y,int sx, int sy,int ex,int ey){
    	if((x >= sx) && (x <= ex) && (y >= sy) && (y <= ey) )
    		return true;
    	else
    		return false;
    }
}