package com.badlogic.androidgames.mrnom;

public class Panel {
	public int x,y,count,color;
	public boolean isdead;
	public Panel(int x, int y, int color) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.color = color;
		this.count = 0;
		this.isdead = true;
	}
	public void kill(){
		this.isdead = true;
	}
}
