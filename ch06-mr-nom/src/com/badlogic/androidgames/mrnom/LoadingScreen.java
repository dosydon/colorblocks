package com.badlogic.androidgames.mrnom;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Screen;
import com.badlogic.androidgames.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.soundenabled = g.newPixmap("soundenabled.png", PixmapFormat.ARGB4444);
        Assets.sounddisabled = g.newPixmap("sounddisabled.png", PixmapFormat.ARGB4444);
        Assets.pause2 = g.newPixmap("pause2.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pause.png", PixmapFormat.ARGB4444);
        Assets.ready2 = g.newPixmap("ready2.png", PixmapFormat.ARGB4444);
        Assets.backtomenu = g.newPixmap("backtomenu.png", PixmapFormat.ARGB4444);
        Assets.cleared = g.newPixmap("cleared.png", PixmapFormat.ARGB4444);
        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.bitten = game.getAudio().newSound("bitten.ogg");
        Assets.menu_background = g.newPixmap("menu_background.png", PixmapFormat.ARGB4444);
        game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void present(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}