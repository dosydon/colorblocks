package com.badlogic.androidgames.mrnom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import android.graphics.Color;



public class PanelWorld {
    static final int WORLD_WIDTH = 5;
    static final int WORLD_HEIGHT = 5;
    static final int SCORE_INCREMENT = 10;
    static final int NUM_COLOR = 3;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;
    static final int COLOR0 = Color.rgb(214, 235, 214);
    static final int COLOR1 = Color.rgb(235,214,215);
    static final int COLOR2 = Color.rgb(214, 235, 235);
    
    public boolean gameOver = false;
    public boolean cleared = false;
    public int score = 0;
    public static Panel panelarray[][] = new Panel[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    static float tick = TICK_INITIAL;
    public static int count;
    public static int num_color0;
    public static int num_color1;
    public static int num_color2;

    public PanelWorld() {
    	for(int i = 0; i < panelarray.length;i++){
        	for(int j = 0; j < panelarray[0].length;j++){
        		panelarray[i][j] = new Panel(i,j,getColor(random.nextInt(3)));
        	}
        }
    	for(int i = 0; i < 2;i++){
    		placePanel(COLOR0);
    		placePanel(COLOR1);
    		placePanel(COLOR2);
    	}
    	count = 0;
    	num_color0 = 2;
    	num_color1 = 2;
    	num_color2 = 2;
    }

    private void placePanel(int color) {
    	
        int panelX = random.nextInt(WORLD_WIDTH);
        int panelY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (panelarray[panelX][panelY].isdead == true)
                break;
            panelX += 1;
            if (panelX >= WORLD_WIDTH) {
                panelX = 0;
                panelY += 1;
                if (panelY >= WORLD_HEIGHT) {
                    panelY = 0;
                }
            }
        }
        panelarray[panelX][panelY].isdead = false;
        panelarray[panelX][panelY].color = color;
        
    }

    public void update(float deltaTime) {
        if (gameOver)
            return;
        if(cleared)
        	return;
        tickTime += deltaTime;

        while (tickTime > tick) {
            tickTime -= tick;
            num_color0 = 0;
            num_color1 = 0;
            num_color2 = 0;
            for(int i = 0; i < panelarray.length;i++){
            	for(int j = 0; j < panelarray[0].length;j++){
            		if(!panelarray[i][j].isdead){
            			count++;
            			if(panelarray[i][j].color == COLOR0){
            				num_color0++;
            			}else if(panelarray[i][j].color == COLOR1){
            				num_color1++;
            			}else if(panelarray[i][j].color == COLOR2){
            				num_color2++;
            			}
            		}
            	}
            }
            
            if(num_color0 < 2 || num_color0>4){
            	gameOver = true;
            	return;
            }
            if(num_color1 < 2 || num_color1>4){
            	gameOver = true;
            	return;
            }
            if(num_color2 < 2 || num_color2>4){
            	gameOver = true;
            	return;
            }
            
            if(cleared = isCleared())
            	return;
            
            if (count > 24) {
                gameOver = true;
                return;
            } else{
            	int color = getColor(random.nextInt(NUM_COLOR));
            	placePanel(color);
            }
            count = 0;
            
        }
        
    }   
    public static int getColor(int n){
    	int color = Color.TRANSPARENT;
    	switch(n){
    	case 0: color = COLOR0;
    		break;
    	case 1: color = COLOR1;
    		break;
    	case 2: color = COLOR2;
    		break;
    	}
    	return color;
    }
    
    public static boolean isCleared(){
    	for(int i = 1;i < WORLD_WIDTH-1;i++){
    		for(int j = 1; j < WORLD_HEIGHT-1;j++){
    			if(!panelarray[i][j].isdead){
    				if(!panelarray[i-1][j-1].isdead){
    					if(!panelarray[i][j-1].isdead){
    						if(!panelarray[i+1][j-1].isdead){
    							if(!panelarray[i-1][j].isdead){
    								if(!panelarray[i+1][j].isdead){
    									if(!panelarray[i-1][j+1].isdead){
    										if(!panelarray[i][j+1].isdead){
    											if(!panelarray[i+1][j+1].isdead){
    						        				return true;
    						        			}
    					        			}
    				        			}
    			        			}
    		        			}
    	        			}
            			}
        			}
    			}
    		}
    	}
    	return false;
    }
}
